// function startTime() {
//     var today = new Date();
//     var h = today.getHours();
//     var m = today.getMinutes();
//     var s = today.getSeconds();
//     m = checkTime(m);
//     s = checkTime(s);
//     document.getElementById('txt').innerHTML =
//         h + ":" + m + ":" + s;
//     var t = setTimeout(startTime, 500);
//
// }
//
//
//
// function checkTime(i) {
//     if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
//     return i;
// }



//
//

function startTimer(){
    var interval = window.setInterval(changeTime, 1000);

    var stopButton = document.getElementById('stop');
    stopButton.addEventListener("click", function(){
        window.clearTimeout(interval);
    });

    var startButton = document.getElementById('resume');
    startButton.addEventListener("click", function(){
        interval = window.setInterval(changeTime, 1000);
    });
}



function changeTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var time = document.getElementById('clock').innerHTML = h + ": " + m + ": " + s;
}
