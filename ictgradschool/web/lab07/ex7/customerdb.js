var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];

document.getElementById('table').innerHTML =buildTable(customers);
document.getElementById('summary').innerHTML=buildSec(customers);

function buildTable(json) {
    var cols = Object.keys(json[0]);   //return a String array with all keys.
    console.log(cols);
    var headerRow = '';
    var bodyRows = '';

    for (x in json[0]){
        headerRow += '<th>' + x + '</th>';
    }

    for (var i=0;i<json.length;i++){
        bodyRows += '<tr>';
        for(var j=0;j<cols.length;j++){
            bodyRows +='<td>' + json[i][cols[j]] + '</td>';
        }
        bodyRows += '</tr>';
    }

// console.log('<thead><tr>'
    //                      + headerRow +
    //                      '</tr></thead><tbody>'
//                      + bodyRows +
//                      '</tbody>');


//Build summary


    return '<thead><tr>'
        + headerRow + '</tr></thead><tbody>'
        + bodyRows + '</tbody>';

}

function buildSec(json) {
    var cols = Object.keys(json[0]);
    var headerRow = '';
    var bodyRows = '';
    var maleCount = 0;
    var femaleCount = 0;
    var age1 = 0;  //age 0-30
    var age2 = 0;  //age 31-64
    var age3 = 0;  //age 65+
    var gold = 0;
    var silver =0;
    var bronze =0;


    headerRow = '<th> male count </th>'+'<th> female count </th>'+'<th> age 0-30</th>'
        + '<th>age 31-64</th>' +  '<th> age 65+ </th>' + '<th> loyalty status - gold </th>'
        + '<th> loyalty status - silver </th>' + '<th> loyalty status - bronze </th>';

    for (var i=0;i<json.length;i++){
//        console.log(json[i]);

        if(json[i].gender==="male"){
            maleCount++;} else{femaleCount++;};
//        console.log("male count: "+ maleCount +"femaleCount :" + femaleCount);
        if((2018 - json[i].year_born)<=30){
            age1++;
        }else if((2018 - json[i].year_born)>30 && (2018 - json[i].year_born)<=64){age2++;}
        else{age3++;}
//loyalty status
        if(json[i].num_hires/((2018-json[i].joined)*54)>=4){
            gold++;}else if(json[i].num_hires/((2018-json[i].joined)*54)>=1&&json[i].num_hires/((2018-json[i].joined)*54)<4){
            silver++}
        else{
            bronze++;}
    }
    bodyRows = '<td>'+maleCount + '</td>'+'<td>'+femaleCount+'</td>'+ '<td>'+ age1+ '</td>'+'<td>'+age2+'<td>'+ age3+'</td>'
        + '<td>'+gold+'</td>' +  '<td>'+silver+ '</td>' + '<td>'+bronze+'</td>';

    return '<thead><tr>'
        + headerRow + '</tr></thead><tbody>'
        + '<tr>'+bodyRows + '</tr></tbody>';
}
